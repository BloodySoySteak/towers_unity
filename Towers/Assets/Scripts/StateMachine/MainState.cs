using UnityEngine;

public class MainState : BaseState
{
    private float rotateTime = 0.5f;
    private float counter = 0.0f;

    public override void PrepareState()
    {
        base.PrepareState();
        Debug.Log(owner.gameObject.name + ": main state");
    }

    public override void UpdateState()
    {
        base.UpdateState();

        counter += Time.deltaTime;
        if (counter >= rotateTime)
        {
            float rndZrot = Random.Range(15.0f, 45.0f);
            Vector3 newRot = owner.gameObject.transform.rotation.eulerAngles;
            newRot.z += rndZrot;
            owner.gameObject.transform.rotation = Quaternion.Euler(newRot);
            counter = 0.0f;
        }
    }
}

using UnityEngine;

public class EndState : BaseState
{
    private float timeToExecute;

    public override void PrepareState()
    {
        base.PrepareState();
        timeToExecute = Random.Range(1.0f, 3.0f);
        Debug.Log(owner.gameObject.name + ": end state, " + timeToExecute);
    }

    public override void UpdateState()
    {
        base.UpdateState();

        timeToExecute -= Time.deltaTime;
        if (timeToExecute <= 0.0f)
        {
            Debug.Log("Destroying");
            Object.Destroy(owner.gameObject);
        }
    }
}

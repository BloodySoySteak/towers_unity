using UnityEngine;

public class Ammo : MonoBehaviour
{
    [SerializeField] private float movementSpeed;

    void Update()
    {
        transform.position += Vector3.forward * Time.deltaTime * movementSpeed;
    }
}
